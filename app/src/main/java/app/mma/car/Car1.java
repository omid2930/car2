package app.mma.car;

public class Car1 {
    int id=0;
    String type="";
    String style="";
    int price=0;
    String imageBase64 ="";
    String image2Base64 ="";
    String image3Base64="";

    public String getImage2Base64() {
        return image2Base64;
    }

    public void setImage2Base64(String image2Base64) {
        this.image2Base64 = image2Base64;
    }

    public String getImage3Base64() {
        return image3Base64;
    }

    public void setImage3Base64(String image3Base64) {
        this.image3Base64 = image3Base64;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }
}
