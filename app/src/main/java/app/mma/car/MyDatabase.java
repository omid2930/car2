package app.mma.car;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;


public class MyDatabase extends SQLiteOpenHelper {
    private static final String DB_NAME = "car";
    private static final int DB_VERSION =3;
    public static final String TABLE_CAR = "tb_car";
    private static final String CMD = "CREATE TABLE IF NOT EXISTS '" + TABLE_CAR + "'('" +
            AppConstonts.id+"'	INTEGER PRIMARY KEY AUTOINCREMENT ,'"+AppConstonts.type
            +"' TEXT,'"+AppConstonts.style+"'	TEXT,'"+AppConstonts.price+"' INTEGER,'"+AppConstonts.dolarprice+"'	INTEGER,'"+
            AppConstonts.engineSize+"' INTEGER,'"+AppConstonts.carModel+"' INTEGER,'"+AppConstonts.arasPelak+"'	INTEGER,'"+
            AppConstonts.sefrKilometr+"' INTEGER,'"+AppConstonts.karKard+"'	INTEGER,'"+AppConstonts.bime+"'	INTEGER ,'" +
            AppConstonts.interiorColor+"' TEXT,'"+AppConstonts.carColor+"'	TEXT,'"+AppConstonts.name+"'  TEXT,'"+
            AppConstonts.phone+"'  INTEGER,'"+AppConstonts.email+"'  TEXT, '"+AppConstonts.description+"' TEXT, '"+
            AppConstonts.image_base64+"' TEXT,'"+AppConstonts.image2_base64+"' TEXT ,'"+AppConstonts.image3_base64+"' TEXT)";


    public MyDatabase(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CMD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_CAR);
        onCreate(db);
    }

    public void insertFlower(Car car) {

        ContentValues values = new ContentValues();
        values.put(AppConstonts.type, car.gettype());
        values.put(AppConstonts.style, car.getStyle());
        values.put(AppConstonts.price, car.getPrice());
        if (car.isDolarprice()) {
            values.put(AppConstonts.dolarprice, 1);
        } else values.put(AppConstonts.dolarprice, 0);
        values.put(AppConstonts.engineSize, car.getEngineSize());
        values.put(AppConstonts.carModel, car.getModelCar());
        if (car.isAraspelak()) {
            values.put(AppConstonts.arasPelak, 1);
        } else values.put(AppConstonts.arasPelak, 0);
        if (car.isSefrKilometr()) {
            values.put(AppConstonts.sefrKilometr, 1);
        } else values.put(AppConstonts.sefrKilometr, 0);
        values.put(AppConstonts.karKard, car.getKarkard());
        values.put(AppConstonts.bime, car.getBime());
        values.put(AppConstonts.interiorColor, car.getInteriorColor());
        values.put(AppConstonts.carColor, car.getCarColor());
        values.put(AppConstonts.name,car.getName());
        values.put(AppConstonts.phone,car.getPhone());
        values.put(AppConstonts.email,car.getEmail());
        values.put(AppConstonts.description,car.getDescription());
        values.put(AppConstonts.image_base64,car.getImage1base64());
        values.put(AppConstonts.image2_base64,car.getImage2Base64());
        values.put(AppConstonts.image3_base64,car.getImage3Base64());
        SQLiteDatabase database=this.getWritableDatabase();
        database.insert(TABLE_CAR,null,values);
        database.close();

    }

    public List<Car1> getFilterSearch(String name,String max_years,String min_years,String max_kilometr,
                                      String min_kkilometr,String price,String brand, int aras) {

        SQLiteDatabase database = this.getReadableDatabase();
        List<Car1> car1List = new ArrayList<>();


            Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_CAR + " WHERE "+AppConstonts.price+"  <"
                    + Integer.valueOf(price) + " AND  "+AppConstonts.arasPelak+"=" + aras, null);
            if (cursor.moveToFirst()) {
                do {
                    Car1 car1 = new Car1();
                    car1.setPrice( cursor.getInt(cursor.getColumnIndex(AppConstonts.price)));
                    car1.setType(cursor.getString(cursor.getColumnIndex(AppConstonts.type)));
                    car1.setImageBase64(cursor.getString(cursor.getColumnIndex(AppConstonts.image_base64)));
                    car1List.add(car1);
                } while (cursor.moveToNext());
            }


        database.close();
        return car1List;

    }
    public List<Car1> getAll(int aras){
        List<Car1> list=new ArrayList<>();
        SQLiteDatabase database =this.getReadableDatabase();
        Cursor cursor=database.rawQuery("SELECT * FROM "+TABLE_CAR+" WHERE "+AppConstonts.arasPelak+"="+aras,null);
        if (cursor.moveToFirst()){
            do {
                Car1 car1=new Car1();
                car1.setId(cursor.getInt(cursor.getColumnIndex(AppConstonts.id)));
                car1.setPrice(cursor.getInt(cursor.getColumnIndex(AppConstonts.price)));
                car1.setType(cursor.getString(cursor.getColumnIndex(AppConstonts.type)));
                car1.setImageBase64(cursor.getString(cursor.getColumnIndex(AppConstonts.image_base64)));
                list.add(car1);
            }while (cursor.moveToNext());
        }
        database.close();
        return list;

    }
    public Car getone(int id){
        Car car=new Car();
        SQLiteDatabase database=this.getReadableDatabase();
        Cursor cursor=database.rawQuery("SELECT * FROM "+TABLE_CAR+" WHERE "+AppConstonts.id+"="+id,null);
        if (cursor.moveToFirst()){
                car.setId(cursor.getInt(cursor.getColumnIndex(AppConstonts.id)));
                car.setPrice(cursor.getInt(cursor.getColumnIndex(AppConstonts.price)));
                car.settype(cursor.getString(cursor.getColumnIndex(AppConstonts.type)));
                car.setStyle(cursor.getString(cursor.getColumnIndex(AppConstonts.style)));
                car.setBime(cursor.getInt(cursor.getColumnIndex(AppConstonts.bime)));
                car.setKarkard(cursor.getInt(cursor.getColumnIndex(AppConstonts.karKard)));
                car.setEngineSize(cursor.getInt(cursor.getColumnIndex(AppConstonts.engineSize)));
                car.setCarColor(cursor.getString(cursor.getColumnIndex(AppConstonts.carColor)));
                car.setInteriorColor(cursor.getString(cursor.getColumnIndex(AppConstonts.interiorColor)));
                car.setDescription(cursor.getString(cursor.getColumnIndex(AppConstonts.description)));
                car.setPhone(cursor.getString(cursor.getColumnIndex(AppConstonts.phone)));
                car.setName(cursor.getString(cursor.getColumnIndex(AppConstonts.name)));
                car.setEmail(cursor.getString(cursor.getColumnIndex(AppConstonts.email)));
                car.setImage1base64(cursor.getString(cursor.getColumnIndex(AppConstonts.image_base64)));
                car.setImage2Base64(cursor.getString(cursor.getColumnIndex(AppConstonts.image2_base64)));
                car.setImage3Base64(cursor.getString(cursor.getColumnIndex(AppConstonts.image3_base64)));
        }
        database.close();
        return car;
    }
    public List<Car1> getFilterKarkard(int karkard,int aras){
        List<Car1> list=new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor=database.rawQuery("SELECT * FROM "+TABLE_CAR+" WHERE "+AppConstonts.arasPelak+"="+aras+" AND "+AppConstonts.sefrKilometr+"="+karkard,null);
        if (cursor.moveToFirst()){
            do {
                Car1 car1=new Car1();
                car1.setId(cursor.getInt(cursor.getColumnIndex(AppConstonts.id)));
                car1.setPrice(cursor.getInt(cursor.getColumnIndex(AppConstonts.price)));
                car1.setType(cursor.getString(cursor.getColumnIndex(AppConstonts.type)));
                car1.setImageBase64(cursor.getString(cursor.getColumnIndex(AppConstonts.image_base64)));
                list.add(car1);
            }while (cursor.moveToNext());
        }
        database.close();
        return list;
    }


}
