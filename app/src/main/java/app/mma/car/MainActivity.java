package app.mma.car;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ViewPager viewPager;
    TabLayout tabLayout;
    Button filter,sefr_kilometr,record,karkarde;
    MyViewPagerAdapter myViewPagerAdapter;
    final int req_filter=1;
    FragmentAras fragmentAras=new FragmentAras();
    FragmentMeli fragmentMeli=new FragmentMeli();
    ImageView imageView;
    MyDatabase openHelper=new MyDatabase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Car car=test();
        //openHelper.insertFlower(car);
        init();
        pager();

    }

    private Car test() {
        Car car=new Car();
        car.setPrice(100);
        car.settype("benz");
        car.setAraspelak(true);
        return car;
    }

    private void pager() {
        myViewPagerAdapter.addFrg(fragmentAras,"ارس");
        myViewPagerAdapter.addFrg(fragmentMeli,"ملی");
        tabLayout=findViewById(R.id.tab_layout);
        viewPager.setAdapter(myViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


    }



    private void init() {
        viewPager=findViewById(R.id.view_pager);
        myViewPagerAdapter=new MyViewPagerAdapter(getSupportFragmentManager());
        filter=findViewById(R.id.Main_button_filter);
        record=findViewById(R.id.main_record);
        karkarde=findViewById(R.id.main_karkard);
        sefr_kilometr=findViewById(R.id.main_sefr);
        record.setOnClickListener(this);
        karkarde.setOnClickListener(this);
        sefr_kilometr.setOnClickListener(this);
        filter.setOnClickListener(this);
        imageView=findViewById(R.id.main_search);
        imageView.setOnClickListener(this);
//        getSupportActionBar().hide();

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==filter.getId()){
            startActivityForResult(new Intent(MainActivity.this,FilterActivity.class),req_filter);
        }else if (v.getId()==record.getId()){
            startActivityForResult(new Intent(MainActivity.this,RecordActivity.class),2);

        }else if (v.getId()==sefr_kilometr.getId()){
            fragmentMeli.FIlterKarkard(1);
            fragmentAras.FIlterKarkard(1);
        }else if (v.getId()==karkarde.getId()){
            fragmentMeli.FIlterKarkard(0);
            fragmentAras.FIlterKarkard(0);
        }else if (v.getId()==imageView.getId()){
            startActivityForResult(new Intent(MainActivity.this,FilterActivity.class),req_filter);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == req_filter) {

            if (resultCode == RESULT_OK) {
                Log.i("filter", "onActivityResult: " + data.getExtras());
                //  int aras=Integer.valueOf(data.getStringExtra("aras"));
                List<String> list = new ArrayList<>();
                list.add(data.getStringExtra("min_year"));
                list.add(data.getStringExtra("max_year"));
                list.add(data.getStringExtra("min_km"));
                list.add(data.getStringExtra("max_km"));
                list.add(data.getStringExtra("brand"));
                list.add(data.getStringExtra("price"));
                //  if (aras==1) { fragmentAras.FilterSearch(list);
                //next fragment
                //if (aras == 0) fragmentMeli.FilterSearch(list);
            }

            }else {
            fragmentAras.update();
            fragmentMeli.update();
        }
        }


    }

