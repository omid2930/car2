package app.mma.car;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {
    EditText input_min_year,input_max_year,input_min_km,input_max_km,input_brand,input_price,input_name;
    Button button;
    Switch aSwitch;
    MyDatabase myDatabase;
    RecyclerView myRecycler;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        myDatabase= new MyDatabase(getApplicationContext());
        myRecycler=findViewById(R.id.list_view_filter);

        init();
        button.setOnClickListener(this);



    }

    private void init() {
        input_min_year=findViewById(R.id.input_min_year);
        input_max_year=findViewById(R.id.input_max_year);
        input_min_km=findViewById(R.id.input_min_km);
        input_max_km=findViewById(R.id.input_max_km);
        input_brand=findViewById(R.id.input_brand);
        input_price=findViewById(R.id.input_filter_price);
        button=findViewById(R.id.filter_button);
        aSwitch=findViewById(R.id.switch_filter);
        input_name=findViewById(R.id.input_name);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==button.getId()){

            if (input_price.getText().toString().length()>0){
//                Intent intent=new Intent();
//                intent.putExtra("min_years",input_min_year.getText().toString().trim());
//                intent.putExtra("max_year",input_max_year.getText().toString().trim());
//                intent.putExtra("min_km",input_min_km.getText().toString().trim());
//                intent.putExtra("max_km",input_max_km.getText().toString().trim());
//                intent.putExtra("brand",input_brand.getText().toString().trim());
//                intent.putExtra("price",input_price.getText().toString().trim());
//                if (aSwitch.isChecked()){
//                    intent.putExtra("aras",1);
//                }else {
//                    intent.putExtra("aras",0);
//                }
//                setResult(RESULT_OK,intent);
//                finish();
                if (aSwitch.isChecked()){
                    showArasPelak();
                }else {
                    showMeliPelak();
                }

            }
            else {
                input_price.requestFocus();
                Toast.makeText(this, "لطفا قیمت را وارد کنید", Toast.LENGTH_SHORT).show();
            }


        }

    }

    private void showMeliPelak() {
        List<Car1> car1List=new ArrayList<>();

        car1List=myDatabase.getFilterSearch(input_name.getText().toString(),
                input_max_year.getText().toString(),
                input_min_year.getText().toString(),
                input_max_km.getText().toString(),
                input_min_km.getText().toString(),
                input_price.getText().toString(),
                input_brand.getText().toString(),
                0);
        recyclerAdapter=new RecyclerAdapter(car1List,FilterActivity.this);
        myRecycler.setAdapter(recyclerAdapter);
        myRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }

    private void showArasPelak() {
        List<Car1> car1List=new ArrayList<>();

        car1List=myDatabase.getFilterSearch(input_name.getText().toString(),
                input_max_year.getText().toString(),
                input_min_year.getText().toString(),
                input_max_km.getText().toString(),
                input_min_km.getText().toString(),
                input_price.getText().toString(),
                input_brand.getText().toString(),
                1);
        recyclerAdapter=new RecyclerAdapter(car1List,FilterActivity.this);
        myRecycler.setAdapter(recyclerAdapter);
        myRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));



    }
}
