package app.mma.car;

public class Car {
    private int id=0;
    private String type="";
    private String style="";
    private int price=0;
    private boolean dolarprice;
    private int EngineSize=0;
    private int modelCar=0;
    private boolean Araspelak;
    private boolean SefrKilometr;
    private int karkard=0;
    private int Bime=0;
    private String InteriorColor="";
    private String CarColor="";
    private String name="";
    private String phone="";
    private String email="";
    private String Description="";
    private String image1base64 ="";
    private String image2Base64="";
    private String image3Base64="";



    public String getImage2Base64() {
        return image2Base64;
    }

    public void setImage2Base64(String image2Base64) {
        this.image2Base64 = image2Base64;
    }

    public String getImage3Base64() {
        return image3Base64;
    }

    public void setImage3Base64(String image3Base64) {
        this.image3Base64 = image3Base64;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String gettype() {
        return type;
    }

    public void settype(String type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isDolarprice() {
        return dolarprice;
    }

    public void setDolarprice(boolean dolarprice) {
        this.dolarprice = dolarprice;
    }

    public int getEngineSize() {
        return EngineSize;
    }

    public void setEngineSize(int engineSize) {
        EngineSize = engineSize;
    }

    public int getModelCar() {
        return modelCar;
    }

    public void setModelCar(int modelCar) {
        this.modelCar = modelCar;
    }

    public boolean isAraspelak() {
        return Araspelak;
    }

    public void setAraspelak(boolean araspelak) {
        Araspelak = araspelak;
    }

    public boolean isSefrKilometr() {
        return SefrKilometr;
    }

    public void setSefrKilometr(boolean sefrKilometr) {
        SefrKilometr = sefrKilometr;
    }

    public int getKarkard() {
        return karkard;
    }

    public void setKarkard(int karkard) {
        this.karkard = karkard;
    }

    public int getBime() {
        return Bime;
    }

    public void setBime(int bime) {
        Bime = bime;
    }

    public String getInteriorColor() {
        return InteriorColor;
    }

    public void setInteriorColor(String interiorColor) {
        InteriorColor = interiorColor;
    }

    public String getCarColor() {
        return CarColor;
    }

    public void setCarColor(String carColor) {
        CarColor = carColor;
    }
    public String getImage1base64() {
        return image1base64;
    }

    public void setImage1base64(String image1base64) {
        this.image1base64 = image1base64;
    }
}
