package app.mma.car;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarteist.autoimageslider.SliderView;

import java.util.Arrays;
import java.util.List;

public class ShowActivity extends AppCompatActivity implements View.OnClickListener {
    TextView type,style,price,bime,karkard,zarfiat,carcolor,InteriorColor,dis;
    ImageView phon,msg,email;
    SliderView show_sliderView;
    SliderAdapter sliderAdapter;
    int id;
    Car car;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            if(extras.containsKey("id")) {
                id = extras.getInt("id");
            }
        }

        init();
        initcar();
        show();
        onclic();
    }

    private void onclic() {
        phon.setOnClickListener(this);
        msg.setOnClickListener(this);
        email.setOnClickListener(this);
    }

    private void show() {
        type.setText(car.gettype());
        style.setText(car.getStyle());
        price.setText(String.valueOf(car.getPrice()));
        bime.setText(String.valueOf(car.getBime()));
        karkard.setText(String.valueOf(car.getKarkard()));
        zarfiat.setText(String.valueOf(car.getEngineSize()));
        carcolor.setText(car.getCarColor());
        InteriorColor.setText(car.getInteriorColor());
        dis.setText(car.getDescription());
        //car_image.setImageBitmap(base64ToBitmap(car));
        ImageSlider imageSlider=new ImageSlider();
        imageSlider.setImage(car.getImage1base64());
        sliderAdapter.addItem(imageSlider);;
        if (!car.getImage2Base64().equals("")){
            ImageSlider imageSlider2=new ImageSlider();
            imageSlider2.setImage(car.getImage2Base64());
            sliderAdapter.addItem(imageSlider2);
        }
        if (!car.getImage3Base64().equals("")){
            ImageSlider imageSlider3=new ImageSlider();
            imageSlider3.setImage(car.getImage3Base64());
            sliderAdapter.addItem(imageSlider3);
        }
        show_sliderView.setSliderAdapter(sliderAdapter);
    }

    private void initcar() {
        MyDatabase openHelper=new MyDatabase(this);
        car = new Car();
        car=openHelper.getone(id);
    }

    private void init() {
        type=findViewById(R.id.show_type);
        style=findViewById(R.id.show_style);
        price=findViewById(R.id.show_price);
        bime=findViewById(R.id.show_bime);
        karkard=findViewById(R.id.show_karkard);
        zarfiat=findViewById(R.id.show_EngineSize);
        carcolor=findViewById(R.id.show_CarColor);
        InteriorColor=findViewById(R.id.show_InteriorColor);
        dis=findViewById(R.id.show_dis);
        phon=findViewById(R.id.show_phone);
        msg=findViewById(R.id.show_msg);
        email=findViewById(R.id.show_email);
        show_sliderView=findViewById(R.id.imageSlider);
        sliderAdapter=new SliderAdapter(ShowActivity.this);
    }
    private Bitmap base64ToBitmap(Car car) {
        byte[] decodedString = Base64.decode(car.getImage1base64(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==phon.getId()){
            String dial="tel:"+car.getPhone();
            Uri uri = Uri.parse(dial);
            Intent intent = new Intent(Intent.ACTION_DIAL, uri);
            startActivity(intent);
        }else if (v.getId()==msg.getId()){
            Uri uri = Uri.parse("smsto:" + car.getPhone());
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(smsIntent);
        }else if (v.getId()==email.getId()){
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{car.getEmail()});
            startActivity(Intent.createChooser(i, "Send mail..."));
        }

    }
}
