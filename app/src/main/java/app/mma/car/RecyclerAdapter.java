package app.mma.car;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>  {
    List<Car1> car1List;
    Activity activity;

    public RecyclerAdapter(List<Car1> car1List,Activity activity) {
        this.car1List = car1List;
        this.activity=activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Car1 car1=car1List.get(position);
        holder.price.setText(String.valueOf(car1.getPrice())+"  تومان");
        holder.type.setText(car1.getType());
        holder.imageView.setImageBitmap(base64ToBitmap(car1));
        holder.id=car1.getId();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(activity,ShowActivity.class);
                intent.putExtra("id",holder.id);
                activity.startActivity(intent);


            }
        });

    }

    private Bitmap  base64ToBitmap(Car1 car1) {
        byte[] decodedString = Base64.decode(car1.getImageBase64(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }


    @Override
    public int getItemCount() {
        return car1List.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder  {
        ImageView imageView;
        TextView price, type;
        TextView carSefr;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.cardview_img);
            price=itemView.findViewById(R.id.cardView_txt_price);
            type =itemView.findViewById(R.id.cardView_txt_model);

        }



    }


}
