package app.mma.car;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.smarteist.autoimageslider.SliderView;

public class FragmentShow extends Fragment implements View.OnClickListener {
    TextView type,style,price,bime,karkard,zarfiat,carcolor,InteriorColor,dis;
    ImageView phon,msg,email;
    SliderView show_sliderView;
    SliderAdapter sliderAdapter;
    int id;
    Car car;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.activity_show,container,false);
        init();
        initcar();
        show();
        onclic();


        return view;

    }
    private void init() {
        type=view.findViewById(R.id.show_type);
        style=view.findViewById(R.id.show_style);
        price=view.findViewById(R.id.show_price);
        bime=view.findViewById(R.id.show_bime);
        karkard=view.findViewById(R.id.show_karkard);
        zarfiat=view.findViewById(R.id.show_EngineSize);
        carcolor=view.findViewById(R.id.show_CarColor);
        InteriorColor=view.findViewById(R.id.show_InteriorColor);
        dis=view.findViewById(R.id.show_dis);
        phon=view.findViewById(R.id.show_phone);
        msg=view.findViewById(R.id.show_msg);
        email=view.findViewById(R.id.show_email);
        show_sliderView=view.findViewById(R.id.imageSlider);
        sliderAdapter=new SliderAdapter(getContext());
    }
    private void initcar() {
        MyDatabase openHelper=new MyDatabase(getContext());
        car = new Car();
        car=openHelper.getone(id);
    }
    private void show() {
        type.setText(car.gettype());
        style.setText(car.getStyle());
        price.setText(String.valueOf(car.getPrice()));
        bime.setText(String.valueOf(car.getBime()));
        karkard.setText(String.valueOf(car.getKarkard()));
        zarfiat.setText(String.valueOf(car.getEngineSize()));
        carcolor.setText(car.getCarColor());
        InteriorColor.setText(car.getInteriorColor());
        dis.setText(car.getDescription());
        //car_image.setImageBitmap(base64ToBitmap(car));
        ImageSlider imageSlider=new ImageSlider();
        imageSlider.setImage(car.getImage1base64());
        sliderAdapter.addItem(imageSlider);;
        if (!car.getImage2Base64().equals("")){
            ImageSlider imageSlider2=new ImageSlider();
            imageSlider2.setImage(car.getImage2Base64());
            sliderAdapter.addItem(imageSlider2);
        }
        if (!car.getImage3Base64().equals("")){
            ImageSlider imageSlider3=new ImageSlider();
            imageSlider3.setImage(car.getImage3Base64());
            sliderAdapter.addItem(imageSlider3);
        }
        show_sliderView.setSliderAdapter(sliderAdapter);
    }
    private void onclic() {
        phon.setOnClickListener(this);
        msg.setOnClickListener(this);
        email.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==phon.getId()){
            String dial="tel:"+car.getPhone();
            Uri uri = Uri.parse(dial);
            Intent intent = new Intent(Intent.ACTION_DIAL, uri);
            startActivity(intent);
        }else if (v.getId()==msg.getId()){
            Uri uri = Uri.parse("smsto:" + car.getPhone());
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(smsIntent);
        }else if (v.getId()==email.getId()){
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{car.getEmail()});
            startActivity(Intent.createChooser(i, "Send mail..."));
        }
    }
}
