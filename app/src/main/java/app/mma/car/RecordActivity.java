package app.mma.car;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class RecordActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    Button button;
    EditText input_type,input_style,input_price,input_EngineSize,input_model,input_karkard,
    input_CarColor,input_InteriorColor,input_name,input_phone,input_email,input_tozih,input_bime;
    Switch aras,km,dolar;
    ImageView imageView1,imageView2,imageView3;
    private static int SELECT_PICTURE = 1;
    String selectedFileBase64;
    String image1Base64="",image2Base64="",image3Base64="";






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        init();
        button.setOnClickListener(this);
        imageView1.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        imageView3.setOnClickListener(this);

    }




    private void init() {
        button=findViewById(R.id.record_button);
        input_type=findViewById(R.id.record_type);
        input_style=findViewById(R.id.record_style);
        input_price=findViewById(R.id.recod_price);
        input_EngineSize=findViewById(R.id.record_hagmkhodro);
        input_model=findViewById(R.id.record_modelcar);
        input_karkard=findViewById(R.id.record_karkard);
        input_CarColor=findViewById(R.id.record_colorcar);
        input_InteriorColor=findViewById(R.id.record_insidercar);
        input_name=findViewById(R.id.record_name);
        input_phone=findViewById(R.id.record_phone);
        input_email=findViewById(R.id.record_email);
        input_tozih=findViewById(R.id.record_tozih);
        aras=findViewById(R.id.record_switch_pe);
        km=findViewById(R.id.record_switch_km);
        dolar=findViewById(R.id.record_switch_dolar);
        input_bime=findViewById(R.id.record_bime);
        imageView1=findViewById(R.id.record_image1);
        imageView2=findViewById(R.id.record_image2);
        imageView3=findViewById(R.id.record_image3);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==imageView1.getId()){
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);


        }else if (v.getId()==imageView2.getId()){
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

        }else if (v.getId()==imageView3.getId()) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

        } else if(v.getId()==button.getId()){
            if (input_type.getText().toString().length()>0){
                if (input_style.getText().toString().length()>0){
                    if (input_price.getText().toString().length()>0){
                        if (input_EngineSize.getText().toString().length() > 0) {
                            if (input_model.getText().toString().length()>0){
                                if (input_CarColor.getText().toString().length()>0){
                                    if (input_InteriorColor.getText().toString().length()>0){
                                        if (input_name.getText().toString().length()>0&input_name.getText().toString().split(" ").length>1){
                                            if (input_phone.getText().toString().length()==11){

                                                if(input_email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")){
                                                    if (!image1Base64.isEmpty()){
                                                        check();
                                                    }else {
                                                        Toast.makeText(this, "لطفا عکس را وارد کنید", Toast.LENGTH_SHORT).show();
                                                    }


                                                }else {
                                                    Toast.makeText(this, "لطفا ایمیل معتبر وارد کنید", Toast.LENGTH_SHORT).show();
                                                    input_email.requestFocus();
                                                }

                                            }else {
                                                Toast.makeText(this, "لطفا شماره تلفن را درست وارد کنید", Toast.LENGTH_SHORT).show();
                                                input_phone.requestFocus();
                                            }

                                        }else {
                                            Toast.makeText(this, "لطفا نام و نام خانوادگی را وارد کنید", Toast.LENGTH_SHORT).show();
                                            input_name.requestFocus();
                                        }

                                    }else {
                                        Toast.makeText(this, "لطفا رنگ داخل ماشین را وارد کنید", Toast.LENGTH_SHORT).show();
                                        input_InteriorColor.requestFocus();
                                    }

                                }else {
                                    Toast.makeText(this, "لطفا رنگ ماشین را وارد کنید", Toast.LENGTH_SHORT).show();
                                    input_CarColor.requestFocus();
                                }
                            }else {
                                Toast.makeText(this, "لطفا مودل خودرو را وارد کنید", Toast.LENGTH_SHORT).show();
                                input_model.requestFocus();
                            }

                        }else {
                            Toast.makeText(this, "لطفا حجم خودرو را وارد کنید", Toast.LENGTH_SHORT).show();
                            input_EngineSize.requestFocus();
                        }

                    }else {
                        Toast.makeText(this, "لطفا قیمت را وارد کنید", Toast.LENGTH_SHORT).show();
                        input_price.requestFocus();
                    }

                }else {
                    Toast.makeText(this, "لطفاتیپ ماشین را وارد کنید", Toast.LENGTH_SHORT).show();
                    input_style.requestFocus();

                }

            }else {
                Toast.makeText(this, "لطفا نوع ماشین را وارد کنید", Toast.LENGTH_SHORT).show();
                input_type.requestFocus();
            }
        }
    }






    private void check() {
        if (km.isChecked()){
            send();
        }else {
            if (input_karkard.getText().toString().length()>0){
                if (input_bime.getText().toString().length()>0){
                    send();

                }else {
                    Toast.makeText(this, "لظفا بیمه را وارد کنید", Toast.LENGTH_SHORT).show();
                    input_bime.requestFocus();
                }

            }else {
                Toast.makeText(this, "لطفا کار کرد را وارد کنید", Toast.LENGTH_SHORT).show();
                input_karkard.requestFocus();
            }
        }
    }

    private void send() {
        Car car=new Car();
        car.settype(input_type.getText().toString().trim());
        car.setStyle(input_style.getText().toString().trim());
        car.setPrice(Integer.parseInt(input_price.getText().toString().trim()));
        car.setEngineSize(Integer.parseInt(input_EngineSize.getText().toString().trim()));
        car.setModelCar(Integer.parseInt(input_model.getText().toString().trim()));
        car.setInteriorColor(input_InteriorColor.getText().toString().trim());
        car.setCarColor(input_CarColor.getText().toString().trim());
        car.setName(input_name.getText().toString());
        car.setPhone(input_phone.getText().toString().trim());
        car.setEmail(input_email.getText().toString().trim());
        if (!km.isChecked()){
            car.setKarkard(Integer.parseInt(input_karkard.getText().toString().trim()));
            car.setBime(Integer.parseInt(input_bime.getText().toString().trim()));
        }
        car.setDolarprice(dolar.isChecked());
        car.setSefrKilometr(km.isChecked());
        car.setAraspelak(aras.isChecked());
        car.setDescription(input_tozih.getText().toString().trim());
        car.setImage1base64(image1Base64);
        car.setImage2Base64(image2Base64);
        car.setImage3Base64(image3Base64);
        MyDatabase openHelper=new MyDatabase(this);
        openHelper.insertFlower(car);
        finish();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                Uri selectedImageURI = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImageURI);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                if (image1Base64.isEmpty()){
                    imageView1.setImageBitmap(yourSelectedImage);
                    imageView1.setScaleType(ImageView.ScaleType.FIT_XY);
                    image1Base64=bitmapToBase64(yourSelectedImage);
                }else if (image2Base64.isEmpty()){
                    imageView2.setImageBitmap(yourSelectedImage);
                    image2Base64=bitmapToBase64(yourSelectedImage);
                    imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                }else if (image3Base64.isEmpty()){
                    imageView3.setImageBitmap(yourSelectedImage);
                    imageView3.setScaleType(ImageView.ScaleType.FIT_XY);
                    image3Base64=bitmapToBase64(yourSelectedImage);
                }


                //selectedFileBase64 =bitmapToBase64(yourSelectedImage);



//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                Bitmap bitmap = yourSelectedImage;
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
//                byte[] imageBytes = byteArrayOutputStream.toByteArray();
//                Log.i("record", "onActivityResult:/ "+imageBytes);


            }


        }

    }
    private String bitmapToBase64(Bitmap bitmap){

        //create a file to write bitmap data
        File f = new File(getApplicationContext().getCacheDir(), "aaa.png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();

//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(f);
//            fos.write(bitmapdata);
//            fos.flush();
//            fos.close();
//        } catch (FileNotFoundException e) {
//            System.out.println("------------> 22222222");
//            e.printStackTrace();
//        } catch (IOException e) {
//            System.out.println("------------> 3333333");
//            e.printStackTrace();
//        }

        return Base64.encodeToString(bitmapdata, Base64.DEFAULT);

    }








}
