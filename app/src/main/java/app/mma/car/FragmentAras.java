package app.mma.car;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class FragmentAras extends Fragment  {
    RecyclerView recyclerView;
    RecyclerAdapter adapter;
    List<Car1> car1List;
    MyDatabase openHelper;
    TextView textView;
    View view;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public List<Car1> getCar1List() {
        return car1List;
    }

    private void init() {
        car1List = openHelper.getAll(1);
        if (car1List.isEmpty())
            ((TextView )view.findViewById(R.id.place_holder_text)).setText("هیچ موردی یافت نشد");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_fragment, container, false);
        recyclerView = view.findViewById(R.id.list_view);
        car1List = new ArrayList<>();
        openHelper=new MyDatabase(getContext());
        init();



        Log.i("carlistA", "onCreateView: "+car1List.size());
        adapter = new RecyclerAdapter(car1List,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return view;
    }

    public void FilterSearch(String name,String max_years,String min_years,String max_kilometr,String min_kilometr,String brand,String price) {
        List<Car1> list1=new ArrayList<>();
        list1=openHelper.getFilterSearch(name,max_years,min_years,max_kilometr,min_kilometr,price,brand,0);
        adapter = new RecyclerAdapter(list1,getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
    public void FIlterKarkard(int SefrKilometr){
        List<Car1> list=new ArrayList<>();
        list=openHelper.getFilterKarkard(SefrKilometr,1);
        if (list.isEmpty())
            ((TextView)view.findViewById(R.id.place_holder_text)).setText("هیچ موردی یافت نشد");
        if(!list.isEmpty())
            ((TextView)view.findViewById(R.id.place_holder_text)).setText("");

        adapter = new RecyclerAdapter(list,getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


    }
    void  update(){
            List<Car1> list=new ArrayList<>();
            list=openHelper.getAll(1);
        if (list.isEmpty())
            ((TextView)view.findViewById(R.id.place_holder_text)).setText("هیچ موردی یافت نشد");
        if(!list.isEmpty())
            ((TextView)view.findViewById(R.id.place_holder_text)).setText("");
            adapter = new RecyclerAdapter(list,getActivity());
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        }


    }

